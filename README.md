# Pic4Review backend

Backend (API + database management) for the [Pic4Review](https://framagit.org/Pic4Carto/Pic4Review) project. It allows missions, users and features data management. A part of the data is also available as a public API.


## Usage

### Dependencies

* NodeJS (>= 8)
* PostgreSQL (>= 9) with PostGIS (>= 2)


### Install

To install Pic4Review backend, you first need a `config.json` file. An example is provided, you can copy and edit it. Make also sure that you have a properly configured PostgreSQL database, and defined credentials in `config.json`.

When done, you can run these commands:
```sh
npm install														# Install dependencies
npm run build													# Generates documentation

# Init database structure (mandatory)
# Should also be done when updating
psql -U youruser -c "CREATE DATABASE yourdatabase"				# Creates new database, you should use your server DB configuration
psql -d yourdatabase -U youruser -f src/init.sql				# Creates database structure for API use

# Testing is optional
psql -d yourdatabase -U youruser -f src/example_data.sql		# Populates database with example data
npm run test													# Launches test suite
```

### Run

Once installed, you can launch the API with these commands:
```sh
# Running on default port 28113
npm run start

# Running on custom port
PORT=1234 npm run start
```

Note that you should run the API in HTTPS to allow secure transactions between client and server.


## License

Copyright 2017 Adrien PAVIE

See [LICENSE](LICENSE.txt) for complete AGPL3 license.

Pic4Review is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pic4Review is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pic4Review. If not, see http://www.gnu.org/licenses/.
