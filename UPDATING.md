# Updating Pic4Review back-end

## Next version (to be released)


## Version 0.5.6

To update your instance, you should run these commands on your PostgreSQL database.

```sql
ALTER TABLE feature ADD COLUMN sourceid VARCHAR;
UPDATE feature SET sourceid = COALESCE(properties->>'id', properties->>'error_id');
```

Then, you should run manually the `src/init.sql` script on your database using `psql` command (or equivalent).


## Version 0.5.4

To update your instance, you should run manually the `src/init.sql` script on your database using `psql` command (or equivalent).


## Version 0.5.3

To update your instance, you should run manually the `src/init.sql` script on your database using `psql` command (or equivalent).


## Version 0.5.0

To update your instance, you should run these commands on your PostgreSQL database.

```sql
-- Complex geometry for features
ALTER TABLE feature ADD COLUMN geomfull GEOMETRY(GEOMETRY, 4326);
UPDATE feature SET geomfull = geom;
ALTER TABLE feature ALTER COLUMN geomfull SET NOT NULL;
CREATE INDEX IF NOT EXISTS feature_geomfull_idx ON feature USING GIST(geomfull);

-- Pictures field in feature table
ALTER TABLE feature ADD COLUMN pictures JSON;
UPDATE feature f SET pictures = p.pictures
FROM feature_picture_cache fp, picture p
WHERE f.id = fp.fid AND fp.pid = p.id;
ALTER TABLE feature ADD COLUMN lastpicedit TIMESTAMP NOT NULL DEFAULT '1970-01-01';

-- Remove picture table
DROP TABLE IF EXISTS picture;
DROP FUNCTION IF EXISTS updateFeaturePictureCache();
DROP TRIGGER IF EXISTS picture_cleaning ON picture;
```

Then, you should run manually the `src/init.sql` script on your database using `psql` command (or equivalent).


## Version 0.4.12

To update your instance, you should run these commands on your PostgreSQL database.

```sql
-- Template column for mission table
ALTER TABLE mission ADD COLUMN template BOOLEAN NOT NULL DEFAULT false;
UPDATE mission SET template = false;
```


## Since commit 969d63aa (21 Aug 2018)

To update your instance since after this commit, you should run manually the `src/init.sql` script on your database using `psql` command (or equivalent).


## Since commit 2786ee64 (3 Aug 2018)

To update your instance since after this commit, you should run these commands on your PostgreSQL database.

```sql
-- Creation date column for mission
ALTER TABLE mission ADD COLUMN created TIMESTAMP NOT NULL DEFAULT current_timestamp;

-- Filling creation date using available data
UPDATE mission SET created = lastedit; -- By default get lastedit data
UPDATE mission m SET created = moment FROM (SELECT mission, MIN(moment) AS moment FROM edit GROUP BY mission) a WHERE a.mission = m.id; -- If available take first edit time
```
