/*
 * Test script for model/Feature.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Hash = require('object-hash');
const Feature = require('../../src/model/Feature');

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;
const TIMEOUT = 10000;

describe("Model > Feature", () => {
	describe("Constructor", () => {
		it("creates the object", () => {
			const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" }, "skipped");
			
			assert.equal(f1.id, 1);
			assert.equal(f1.coordinates[0], 48);
			assert.equal(f1.coordinates[1], -1.7);
			assert.equal(f1.properties.t1, "v1");
			assert.equal(f1.status, "skipped");
			assert.ok(f1.geom.isValid());
			assert.equal(f1.geom.getCoordinate().x, -1.7);
			assert.equal(f1.geom.getCoordinate().y, 48);
		});
		
		it("creates the object with defaults values", () => {
			const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" });
			
			assert.equal(f1.id, 1);
			assert.equal(f1.coordinates[0], 48);
			assert.equal(f1.coordinates[1], -1.7);
			assert.equal(f1.properties.t1, "v1");
			assert.equal(f1.status, "new");
			assert.ok(f1.geom.isValid());
			assert.equal(f1.geom.getCoordinate().x, -1.7);
			assert.equal(f1.geom.getCoordinate().y, 48);
		});
		
		it("fails if no ID is given", () => {
			assert.throws(() => {
				const f1 = new Feature(null, [ 48, -1.7 ], { "t1": "v1" });
			}, TypeError);
		});
		
		it("fails if coordinates are not a float array", () => {
			assert.throws(() => {
				const f1 = new Feature(1, "not an array", { "t1": "v1" });
			}, TypeError);
		});
		
		it("fails if coordinates are not of correct length", () => {
			assert.throws(() => {
				const f1 = new Feature(1, [ 1, 2, 3 ], { "t1": "v1" });
			}, TypeError);
		});
		
		it("fails if status is invalid", () => {
			assert.throws(() => {
				const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" }, "not a valid status");
			}, TypeError);
		});
	});
	
	describe("get status", () => {
		it("returns status", () => {
			const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" }, "skipped");
			assert.equal(f1.status, "skipped");
		});
	});
	
	describe("set status", () => {
		it("works if status is valid", () => {
			const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" });
			
			assert.equal(f1.status, "new");
			
			f1.status = "nopics";
			
			assert.equal(f1.status, "nopics");
		});
		
		it("fails if status is not valid", () => {
			assert.throws(() => {
				const f1 = new Feature(1, [ 48, -1.7 ], { "t1": "v1" });
				f1.status = "not a valid status";
			}, TypeError);
		});
	});
	
	describe("asGeoJSON", () => {
		it("returns GeoJSON representation", () => {
			const f1 = new Feature(1, [ 48.1294, -1.6760 ], { "t1": "v1" });
			const res = f1.asGeoJSON();
			assert.equal(res.type, "Feature");
			assert.equal(res.geometry.type, "Point");
			assert.equal(res.geometry.coordinates[0], -1.6760);
			assert.equal(res.geometry.coordinates[1], 48.1294);
			assert.equal(res.properties.t1, "v1");
		});
	});
});
