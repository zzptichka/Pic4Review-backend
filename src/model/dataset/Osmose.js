const Dataset = require('../Dataset');
const Feature = require('../Feature');
const Hash = require('object-hash');
const OsmoseRequest = require('osmose-request');
const request = require('request-promise-native');

/**
 * An Osmose {@link Dataset} is a set of features retrieved from {@link http://osmose.openstreetmap.fr|Osmose API}.
 * Osmose is a system returning anomalies from OpenStreetMap database. It also supports open data merging with OSM.
 * This kind of dataset is dynamic.
 * 
 * @param {int} itemId The Osmose item ID
 * @param {int} amount The amount of errors to review
 * @param {Object} [options] Options for data retrieval
 * @param {int} [options.class] Osmose class for a given item
 * @param {LatLngBounds} [options.bbox] Bounding box for limiting search area
 */
class OsmoseDataset extends Dataset {
	constructor(itemId, amount, options) {
		super();
		
		this.options = options || {};
		amount = amount || 10000;
		
		if(isNaN(parseInt(itemId))) {
			throw new TypeError("You should provide a valid item ID");
		}
		else if(isNaN(parseInt(amount)) || amount < 1) {
			throw new TypeError("You should provide a valid amount (integer > 0)");
		}
		
		this.id = Hash(itemId);
		this.searchOptions = Object.assign({ item: itemId, limit: amount, status: "open", full: true }, this.options);
		this.features = null;
		
		//Convert bbox
		if(this.options.bbox) {
			this.searchOptions.bbox = this.options.bbox.getWest()+","+this.options.bbox.getSouth()+","+this.options.bbox.getEast()+","+this.options.bbox.getNorth();
		}
		
		this._loadData();
	}
	
	/**
	 * Download data from Osmose API
	 * @private
	 */
	_loadData() {
		this.isDownloading = true;
		
		// Using library
// 		(new OsmoseRequest())
// 		.fetchErrors(this.searchOptions)
		
		// Using beta endpoint
		const url = "https://beta.osmose.openstreetmap.fr/fr/api/0.3beta/issues?" + Object.entries(this.searchOptions).map(e => e[0]+"="+e[1]).join("&");
		request(url)
		.then(res => JSON.parse(res))
		.then(res => res.issues)
		.then(result => {
			this.features = [];
			const ignoreProps = [ "lat", "lon", "item", "class", "classs", "level", "elems", "subtitle", "source", "subclass", "username", "update", "id" ];
			
			for(const f of result) {
				//Filter properties to only display what's useful
				const props = {};
				for(const k in f) {
					if(ignoreProps.indexOf(k) < 0) {
						props[k] = f[k];
					}
					else if(k === "id") {
						props["error_id"] = f[k];
					}
					else if(k === "elems" && f[k].trim().length > 0) {
						const osmid = f[k].split("_")[0];
						props["id"] = osmid.replace(/^([a-z]+)(\d+)$/, "$1/$2");
					}
					else if(k === "subtitle" && f[k].trim().length > 0) {
						props["details"] = f[k];
					}
				}
				
				const ownId = f.item + "_" + Hash(f.lat+","+f.lon); // Custom ID to handle unstability of Osmose IDs
				
				this.features.push(new Feature(ownId, [ parseFloat(f.lat), parseFloat(f.lon) ], props));
			}
			
			this.isDownloading = false;
		})
		.catch(e => {
			console.error(e);
			this.isDownloading = false;
		});
	}
	
	/**
	 * Override of {@link Dataset#getAllFeatures}
	 */
	getAllFeatures() {
		return new Promise((resolve, reject) => {
			if(this.isDownloading) {
				setTimeout(() => {
					resolve(this.getAllFeatures());
				}, 100);
			}
			else {
				if(this.features) {
					resolve(this.features);
				}
				else {
					reject(new Error("Features not available"));
				}
			}
		});
	}
	
	/**
	 * Retrieve tags to apply to solve an Osmose error
	 * @param {string} id The Osmose error ID
	 * @return {Promise} A promise resolving on tags to apply
	 */
	static getErrorTags(id) {
		return (new OsmoseRequest())
			.fetchError(id)
			.then(result => {
				const tags = {};
				
				if(result.new_elems && result.new_elems.length > 0 && result.new_elems[0].add) {
					result.new_elems[0].add.forEach(e => {
						tags[e.k] = e.v;
					});
				}
				
				return tags;
			});
	}
	
	/**
	 * Mark an error as fixed on Osmose
	 * @param {string} id The Osmose error ID
	 * @return {Promise} A promise resolving when done
	 */
	static setErrorFixed(id) {
		return (new OsmoseRequest()).closeError(id);
	}
}

module.exports = OsmoseDataset;
