const CONFIG = require("../../config.json");
const P4C = require("pic4carto");
const Bottleneck = require("bottleneck");
const jsts = require("jsts");

const BUFFER_DIST_METERS = 20;
const TIME_INTERVAL_MS = 1000*60*60*24*365*3; //Back to 3 years

const delayer = new Bottleneck({
	maxConcurrent: 5,
	minTime: 100
});

const mapCoordsArray = (arr, fct) => {
	if(arr.length === 2 && typeof arr[0] === "number" && typeof arr[1] === "number") {
		return fct(arr[0], arr[1]);
	}
	else {
		return arr.map(a => mapCoordsArray(a, fct));
	}
};

const cvt4326to3857 = (lon,lat) => {
	const x = lon * 20037508.34 / 180;
	let y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
	y = y * 20037508.34 / 180;
	return [x, y];
};

const cvt3857to4326 = (x, y) => {
	const lon = x *  180 / 20037508.34;
	const lat = Math.atan(Math.exp(y * Math.PI / 20037508.34)) * 360 / Math.PI - 90;
	return [lon, lat];
};

/**
 * The pictures store is a class able to retrieve pictures.
 * Pictures can be cached or retrieved from various API online.
 */
class PicturesStore {
	constructor() {
		this.statuses = [];
		
		const opts = {};
		if(CONFIG.pictureFetchersCredentials) {
			opts.fetcherCredentials = CONFIG.pictureFetchersCredentials;
		}
		
		this.picman = new P4C.PicturesManager(opts);
		this.geomFactory = new jsts.geom.GeometryFactory();
	}
	
	/**
	 * Starts retrieving pictures around the given features.
	 * @param {Feature[]} features The list of features
	 * @return {Object} An object, as { token: int, ready: promise } where token can be used for watching progress, and ready a promise resolving on feature status and pictures when pictures are ready.
	 */
	getPicturesForFeatures(features) {
		//Create token
		const token = this.statuses.push({
			ready: 0,
			total: features.length
		}) - 1;
		
		const result = [];
		
		const promises = features.map(f => {
			return new Promise(resolve => {
				const geom = f.asGeoJSON().geometry;
				
				this._getPicture(geom)
				.then(pictures => {
					result.push({ id: f.id, status: pictures.length > 0 ? 'new' : 'nopics', geom: geom, coordinates: f.coordinates, pictures: pictures });
					this._updateStatus(token);
					resolve();
				})
				.catch(e => {
					console.error(e);
					this._updateStatus(token);
					resolve();
				});
			});
		});
		
		const p = Promise.all(promises)
		.then(() => {
			return result;
		});
		
		return { ready: p, token: token };
	}
	
	/**
	 * Starts retrieving pictures around features of a certain mission.
	 * @param {int} mid The mission ID
	 * @param {boolean} [updateMission] Set mission as online after retrieval (defaults to false)
	 * @return {Object} An object, as { token: int, ready: promise } where token can be used for watching progress, and ready a promise resolving when pictures are ready.
	 */
	preparePicturesForMission(mid, updateMission) {
		updateMission = updateMission || false;
		
		//Create token
		const token = this.statuses.push({
			ready: 0,
			total: -1,
			locked: false,
			mission: mid
		}) - 1;
		
		//Retrieve features
		const p = db.query("SELECT id, ST_AsGeoJSON(geomfull) AS geojson, pictures, status, lastpicedit FROM feature WHERE mission = $1", [ mid ])
		.then(d => {
			const picTooOldDate = Date.now() - 1000*60*60*2;
			
			this.statuses[token].total = d.rows.length;
			this.statuses[token].ready = d.rows.filter(l => l.status === "reviewed" || l.lastpicedit.getTime() >= picTooOldDate).length;
			
			const deleteSkips = [];
			
			//Update pictures
			const promises = d.rows
			.filter(l => l.status !== "reviewed" && l.lastpicedit.getTime() < picTooOldDate)
			.map((l, i) => {
				return new Promise((resolve, reject) => {
					l.geojson = JSON.parse(l.geojson);
					
					this._getPicture(l.geojson)
					.then(p => {
						//Did pictures list changed ?
						if(
							!l.pictures
							|| p.length !== l.pictures.length
							|| !l.pictures.map(pic => pic.pictureUrl).join("/").startsWith(p.map(pic => pic.pictureUrl).join("/"))
						) {
							const status = p && p.length > 0 ? "new" : "nopics";
							db.query("UPDATE feature SET status = $1, pictures = $2, lastpicedit = current_timestamp WHERE id = $3", [ status, JSON.stringify(p), l.id ])
							.then(() => {
								deleteSkips.push(l.id);
								this._updateStatus(token);
								resolve();
							});
						}
						else {
							db.query("UPDATE feature SET lastpicedit = current_timestamp WHERE id = $1", [ l.id ])
							.then(() => {
								this._updateStatus(token);
								resolve();
							});
						}
					})
					.catch(e => {
						console.error(e);
						this._updateStatus(token);
						resolve();
					});
				});
			});
			
			//Handling next actions
			const nextActions = () => {
				const nextActionsPromises = [];
				
				if(deleteSkips.length > 0) {
					nextActionsPromises.push(db.query("DELETE FROM feature_skip WHERE feature IN ("+deleteSkips.join(", ")+")"));
				}
				
				if(updateMission) {
					nextActionsPromises.push(db.query("UPDATE mission SET status='online', lastedit = current_timestamp WHERE id = $1", [ mid ]));
				}
				
				return Promise.all(nextActionsPromises)
				.then(() => {
					this.statuses[token].ready = this.statuses[token].total;
					return true;
				})
				.catch(e => {
					console.error(e);
					this.statuses[token].ready = this.statuses[token].total;
				});
			};
			
			//Notify pictures are ready
			return Promise.all(promises)
			.then(nextActions)
			.catch(e => {
				console.error(e);
				nextActions();
			});
		})
		.catch(e => {
			console.error(e);
			this.statuses[token].ready = this.statuses[token].total;
		});
		
		return { ready: p, token: token };
	}
	
	_updateStatus(token, increment) {
		if(this.statuses[token].locked) {
			setTimeout(() => this._updateStatus(token, increment), 10);
		}
		else {
			this.statuses[token].locked = true;
			increment = increment || 1;
			this.statuses[token].ready += increment;
			this.statuses[token].locked = false;
			
			if(this.statuses[token].mission && (this.statuses[token].ready % 10 === 0 || this.statuses[token].ready === this.statuses[token].total)) {
				console.log("[PictureStore]", "Mission", this.statuses[token].mission, ":", this.statuses[token].ready, "/", this.statuses[token].total);
			}
		}
	}
	
	/**
	 * Ask about the progress of a pictures retrieval call.
	 * @param {int} token The token
	 * @return {int} The progress, in % (or -1 if token is invalid)
	 */
	getRetrievalStatus(token) {
		return this.statuses[token] ? this.statuses[token].ready / this.statuses[token].total * 100 : -1;
	}
	
	/**
	 * Retrieve picture around coordinates
	 * @param {Object} geom The feature GeoJSON geometry
	 * @return {Promise} Resolves on pictures
	 * @private
	 */
	_getPicture(geom) {
		return delayer.schedule(
			{ expiration: 1000*30 },
			() => {
				if(geom.type === "Point") {
					return this.picman
					.startPicsRetrievalAround(
						new P4C.LatLng(geom.coordinates[1], geom.coordinates[0]),
						BUFFER_DIST_METERS,
						{
							towardscenter: true,
							mindate: Date.now() - TIME_INTERVAL_MS
						}
					);
				}
				else {
					//Get bounding box around buffer
					const buffer = (new jsts.io.GeoJSONReader()).read(this._geomToBuffer(geom));
					const envl = buffer.getEnvelopeInternal();
					
					//Retrieve pics in this box
					return this.picman
					.startPicsRetrieval(
						new P4C.LatLngBounds(new P4C.LatLng(envl.getMinY(), envl.getMinX()), new P4C.LatLng(envl.getMaxY(), envl.getMaxX())),
						{
							mindate: Date.now() - TIME_INTERVAL_MS
						}
					)
					.then(pics => {
						//Only send pics in buffer around feature geometry
						return pics.filter(p => {
							return buffer.contains(
								this.geomFactory.createPoint(
									new jsts.geom.Coordinate(
										p.coordinates.lng,
										p.coordinates.lat
									)
								)
							);
						});
					});
				}
			}
		);
	}
	
	/**
	 * Transforms a feature geometry into a wider buffer
	 * @param {Object} geom The feature GeoJSON geometry
	 * @return {Object} The GeoJSON of the buffer
	 */
	_geomToBuffer(geom) {
		//TODO Would be better if jsts offered projection transform 4326 <-> 3857
		let g = (new jsts.io.GeoJSONReader()).read({ type: geom.type, coordinates: mapCoordsArray(geom.coordinates, cvt4326to3857) });
		g = g.buffer(BUFFER_DIST_METERS);
		const res = (new jsts.io.GeoJSONWriter()).write(g);
		res.coordinates = mapCoordsArray(res.coordinates, cvt3857to4326);
		return res;
	}
}

module.exports = PicturesStore;
