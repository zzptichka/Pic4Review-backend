const Osmose = require("../model/dataset/Osmose");
const P4C = require("pic4carto");

/**
 * Route handler for features-related routes.
 * @name MissionsFeaturesRouteHandler
 */

/**
 * Handle /missions/:mid/features requests.
 */
exports.list = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else {
		const mid = parseInt(req.params.mid);
		
		db.query("SELECT id, status, ST_AsGeoJSON(geom) AS geom FROM feature WHERE mission = $1", [ mid ])
		.then(d => {
			if(d.rows.length > 0) {
				res.status(200).send({ features: d.rows.map(o => { o.geom = JSON.parse(o.geom); return o; }) });
			}
			else {
				res.status(404).send({ "error": "No mission with given ID", "details_for_humans": "This mission has no features associated ! If you created this mission, please try to re-create it, as an error might happened" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "Can't find this mission, are you sure that it's still online ?" });
		});
	}
};

/**
 * Handle /missions/:mid/features/next requests.
 */
exports.next = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(req.query && req.query.lat && isNaN(parseFloat(req.query.lat))) {
		res.status(400).send({ "error": "Invalid latitude" });
	}
	else if(req.query && req.query.lng && isNaN(parseFloat(req.query.lng))) {
		res.status(400).send({ "error": "Invalid longitude" });
	}
	else {
		const mid = parseInt(req.params.mid);
		const params = [ mid ];
		let query = "SELECT f.id, f.properties, f.status, f.pictures, ST_AsGeoJSON(f.geom) AS geom, ST_AsGeoJSON(f.geomfull) AS geomfull "
					+"FROM feature f ";
		
		if(req.query.userid) {
			query += `LEFT JOIN feature_skip s ON f.id = s.feature AND s.userid = $${params.length+1} `;
			params.push(req.query.userid);
		}
		
		query += "WHERE f.mission = $1 AND f.status='new' AND f.pictures IS NOT NULL ";
		
		if(req.query.userid) {
			query += "AND s.id IS NULL ";
		}
		
		if(req.query.lng && req.query.lat) {
			query += `ORDER BY f.geom <-> ST_SetSRID(ST_Point($${params.length+1}, $${params.length+2}), 4326) `;
			params.push(req.query.lng);
			params.push(req.query.lat);
		}
		else {
			query += "ORDER BY random() ";
		}
		
		query += "LIMIT 1";
		
		db.query(query, params)
		.then(d => {
			if(d.rows.length === 1) {
				const result = d.rows[0];
				result.geom = JSON.parse(result.geom);
				
				// Check if further info is necessary
				db.query("SELECT datatype, (dataoptions->>'editors')::json AS editors, dataoptions->>'item' AS item FROM mission WHERE id = $1", [ req.params.mid ])
				.then(d2 => {
					if(d2.rows.length === 1) {
						const missionInfo = d2.rows[0];
						
						// Osmose import mission -> needs tags to apply on feature
						if(
							missionInfo.datatype === "osmose"
							&& missionInfo.editors
							&& missionInfo.editors.type === "importer"
							&& result.properties.error_id
						) {
							// Fetch info from Osmose
							Osmose
								.getErrorTags(result.properties.error_id)
								.then(tags => {
									result.properties = Object.assign(result.properties, tags);
									res.status(200).send({ feature: result });
								})
								.catch(e => {
									// Possibly due to out-of-sync mission
									// We try to find a new item if its ID changed
									const osmoseDS = new Osmose(
										missionInfo.item,
										1,
										{ bbox: new P4C.LatLngBounds(
											new P4C.LatLng(result.geom.coordinates[1]-0.00001, result.geom.coordinates[0]-0.00001),
											new P4C.LatLng(result.geom.coordinates[1]+0.00001, result.geom.coordinates[0]+0.00001)
										) }
									);
									
									osmoseDS.getAllFeatures()
									.then(features => {
										// New feature found, we replace the old one
										if(features.length > 0) {
											const newFeature = features[0];
											
											db.query("UPDATE feature SET properties = $1 WHERE id = $2", [ newFeature.properties, result.id ])
											.then(() => {
												// Call again our API to reuse previous code
												// Also we force coordinates to look for the same feature
												req.query.lng = result.geom.coordinates[0];
												req.query.lat = result.geom.coordinates[1];
												exports.next(req, res);
											})
											.catch(e => {
												console.error(e);
												res.status(500).send({ "error": "Failed to update Osmose feature", "details_for_humans": "There was a technical issue when looking for this feature details. Please retry later." });
											});
										}
										// No feature found, we mark it as solved
										else {
											db.query("UPDATE feature SET status='reviewed' WHERE id = $1", [ result.id ]);
											
											// Try to find another feature to send
											delete req.query.lat;
											delete req.query.lng;
											exports.next(req, res);
										}
									})
									.catch(e => {
										console.error(e);
										res.status(500).send({ "error": "Failed to retrieve Osmose tags", "details_for_humans": "Oops ! Osmose API, which is used for getting the feature to review, seems unavailable. Please retry later." });
									});
								});
						}
						else {
							res.status(200).send({ feature: result });
						}
					}
					else {
						throw new Error("Mission not found");
					}
				})
				.catch(e => {
					console.error(e);
					res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "Can't find this mission, is it still online ?" });
				});
			}
			else {
				res.status(200).send({ "info": "No more new feature", "details_for_humans": "It seems that all features are already reviewed, but you can check out another mission." });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission feature", "details_for_humans": "Can't find any feature for this mission, is it still online ?" });
		});
	}
};

/**
 * Handle /missions/:mid/features/:fid requests.
 */
exports.update = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(!req.params.fid || isNaN(parseInt(req.params.fid))) {
		res.status(400).send({ "error": "Invalid feature ID" });
	}
	else if(!req.query || !req.query.username || !req.query.userid) {
		res.status(400).send({ "error": "Invalid user info" });
	}
	else if(!req.query.status || ["reviewed", "cantsee", "skipped"].indexOf(req.query.status) < 0) {
		res.status(400).send({ "error": "Invalid new feature status" });
	}
	else {
		const mid = parseInt(req.params.mid);
		const fid = parseInt(req.params.fid);
		
		db.query("SELECT status, properties FROM feature WHERE id = $1 AND mission = $2", [ fid, mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const prevStatus = d.rows[0].status;
				const props = d.rows[0].properties;
				
				//Change feature status
				if(req.query.status === "reviewed" || req.query.status === "cantsee") {
					db.query("UPDATE feature SET status = $1, lastedit = current_timestamp WHERE id = $2 AND mission = $3", [ req.query.status, fid, mid ])
					.then(() => {
						//Create user edit
						db.query(
							"INSERT INTO edit(userid, username, prevstatus, newstatus, mission, feature) VALUES($1, $2, $3, $4, $5, $6)",
								[ req.query.userid, req.query.username, prevStatus, req.query.status, mid, fid ]
						)
						.then(() => {
							res.status(200).send({ "info": "Update successful" });
						})
						.catch(e => {
							console.error(e);
							res.status(500).send({ "error": "Failed to save edit", "details_for_humans": "I'm sorry, I saved your edit but can't count it in your statistics due to an error..." });
						});
					})
					.catch(e => {
						console.error(e);
						res.status(500).send({ "error": "Failed to update feature", "details_for_humans": "I wasn't able to save your edit, can you retry a bit later ?" });
					});
					
					// Parallel processing : update feature elsewhere
					if(req.query.status === "reviewed" && props.error_id) {
						// Check if further info is necessary
						db.query("SELECT datatype, (dataoptions->>'editors')::json AS editors FROM mission WHERE id = $1", [ mid ])
						.then(d2 => {
							if(d2.rows.length === 1) {
								const missionInfo = d2.rows[0];
								
								// Osmose import mission -> needs tags to apply on feature
								if(
									missionInfo.datatype === "osmose"
									&& missionInfo.editors
									&& missionInfo.editors.type === "importer"
								) {
									// Fetch info from Osmose
									Osmose
										.setErrorFixed(props.error_id)
										.then(() => {})
										.catch(e => {
											console.error(e);
										});
								}
							}
						})
						.catch(e => {
							console.error(e);
						});
					}
				}
				else if(req.query.status === "skipped") {
					db.query(
						"INSERT INTO feature_skip(userid, username, mission, feature) VALUES($1, $2, $3, $4)",
						[ req.query.userid, req.query.username, mid, fid ]
					)
					.then(() => {
						res.status(200).send({ "info": "Update successful" });
					})
					.catch(e => {
						console.error(e);
						res.status(500).send({ "error": "Failed to update feature", "details_for_humans": "I wasn't able to save your edit, can you retry a bit later ?" });
					});
				}
			}
			else {
				res.status(400).send({ "error": "Can't find given feature", "details_for_humans": "I can't find this feature, are you sure the mission is still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve given feature", "details_for_humans": "Something went wrong when looking for this feature, can you retry a bit later ?" });
		});
	}
};


/**
 * Handle /missions/:mid/export/missing calls.
 */
exports.exportMissing = function(req, res) {
	const validFormats = [ "gpx", "geojson", "kml" ];
	
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params || !req.query) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(!req.query.format || validFormats.indexOf(req.query.format) < 0) {
		res.status(400).send({ "error": "Invalid export format" });
	}
	else {
		const mid = parseInt(req.params.mid);
		
		db.query("SELECT ST_X(f.geom) AS lon, ST_Y(f.geom) AS lat, COALESCE(f.properties->>'name', f.properties->>'title', f.properties->>'id', 'Feature') AS label FROM feature f WHERE f.mission = $1 AND (f.status='nopics' OR f.pictures IS NULL)", [ mid ])
		.then(d => {
			if(d.rows.length > 0) {
				let result = null;
				
				switch(req.query.format) {
					case "geojson":
						const g = { type: "FeatureCollection", features: d.rows.map(l => {
							return { type: "Feature", geometry: { type: "Point", coordinates: [ l.lon, l.lat ] }, properties: { name: l.label } };
						}) };
						result = JSON.stringify(g);
						break;
					
					case "gpx":
						result = '<?xml version="1.0" encoding="UTF-8"?>\n'
						result += '<gpx version="1.1" creator="Pic4Review" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">\n';
						
						d.rows.forEach(l => {
							result += '<wpt lat="'+l.lat+'" lon="'+l.lon+'"><name>'+l.label+'</name></wpt>\n';
						});
						
						result += '</gpx>\n';
						break;
					
					case "kml":
						result = '<?xml version="1.0" encoding="UTF-8"?>\n'
						result += '<kml xmlns="http://www.opengis.net/kml/2.2">\n';
						result += '<Document>\n';
						
						d.rows.forEach(l => {
							result += '<Placemark><name>'+l.label+'</name><Point><coordinates>'+l.lon+','+l.lat+',0</coordinates></Point></Placemark>\n';
						});
						
						result += '</Document>\n';
						result += '</kml>\n';
						break;
				}
				
				res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=missing.'+req.query.format});
				res.end(result);
			}
			else {
				res.status(200).send({ "info": "No features without pictures", "details_for_humans": "This mission doesn't have any feature lacking pictures for the while (that's a good news !), you can directly review features !" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "I can't find this mission, can you retry a bit later ?" });
		});
	}
};
