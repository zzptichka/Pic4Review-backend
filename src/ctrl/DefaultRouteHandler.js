/**
 * Route handler for default routes, like getting API status.
 * @name DefaultRouteHandler
 */


exports.status = function(req, res) {
	res.json({ "status": "OK" });
};
