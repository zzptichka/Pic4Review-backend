/**
 * Application start point.
 * @name App
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const CONFIG = require('../config.json');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const DbManager = require('./ctrl/DbManager');
const TimeManager = require('./ctrl/TimeManager');

const app = express();
const port = process.env.PORT || 28113;

Date.prototype.toP4R = function() {
	var mm = this.getMonth() + 1; // getMonth() is zero-based
	var dd = this.getDate();
	
	return [this.getFullYear(),
	(mm>9 ? '' : '0') + mm,
	(dd>9 ? '' : '0') + dd
	].join('-');
};

app.use(cors());
app.options('*', cors());

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '20mb' }));
app.use('/doc', express.static(__dirname+'/../doc/api'));
app.use('/jsdoc', express.static(__dirname+'/../doc/js'));

const server = app.listen(port, () => {
	console.log('API started on port: ' + port);
});

//Setup DB
const db = new DbManager(CONFIG.db.host, CONFIG.db.user, CONFIG.db.pass, CONFIG.db.name, CONFIG.db.port);
db.initDb()
.then(() => {
	global.db = db;
	
	//Setup API
	const routes = require('./Routes');
	routes(app);
	
	//Log HTTP queries
	app.use((req, res, next) => {
		db.logHttpQuery(req, res, next);
	});
	
	//404
	app.use((req, res) => {
		res.status(404).send({url: req.originalUrl + ' not found'})
	});
	
	//Various maintenance services
	const timemgr = new TimeManager();
	setTimeout(() => timemgr.startMissionUpdateService(), CONFIG.timers.firstMissionUpdate*1000);
	setTimeout(() => timemgr.startMissionRankUpdateService(), 10*60*1000); // Started later because mission rank cache is updated after inactive mission disabling
	timemgr.startInactiveMissionDisablingService();
	timemgr.startPictureProviderUpdateService();
	
	server.p4rReady = true;
})
.catch(e => {
	throw e;
});

module.exports = server;
